'use strict';

module.exports = {

    // Map a mdm Transaction object to a Hubspot Transaction object
    mapMdmTransactionToHs: function(mdmTransaction) {
        let hsTransaction = {
            firstname: mdmTransaction.prenom,
            lastname: mdmTransaction.nom,
            email: mdmTransaction.email,
            zip: mdmTransaction.zip,
            animal: mdmTransaction.animal
        }
        return hsTransaction;
    }

}