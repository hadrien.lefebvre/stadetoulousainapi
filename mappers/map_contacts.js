'use strict';

module.exports = {

    // Map a mdm Contact object to a Hubspot Contact object
    mapMdmContactToHs: function(mdmContact) {
        let hsContact = {
            firstname: mdmContact.prenom,
            lastname: mdmContact.nom,
            email: mdmContact.email,
            zip: mdmContact.zip,
            animal: mdmContact.animal
        }
        return hsContact;
    },

    // Map a Hubspot Contact object to a Mdm Contact object
    mapHsContactToMdm: function(hsContact) {
        let mdmContact = {
            firstName : hsContact.prenom,
            lastName : hsContact.nom,
            email : hsContact.email,
        }
        return mdmContact;
    }

}