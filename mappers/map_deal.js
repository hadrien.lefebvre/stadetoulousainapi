'use strict';

module.exports = {

    // Map a mdm Product object to a Hubspot Product custom object
    mapMdmProductToHs: function(mdmProduct) {
        let hsProduct = {
            firstname: mdmProduct.prenom,
            lastname: mdmProduct.nom,
            email: mdmProduct.email,
            zip: mdmProduct.zip,
            animal: mdmProduct.animal
        }
        return hsProduct;
    },

}