'use strict';

const mappers = require('../../mappers/map_contacts')
const moment = require('moment');
const AWS = require('aws-sdk');

// TODO variable for those
const s3 = new AWS.S3({
  accessKeyId: 'AKIAVONT4AWXZ2AEWW3Q',
  secretAccessKey: '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',
});

module.exports.create_s3_contact = async (event) => {
    let response;
    //TODO : variable for those
    const bucketName = 'stadetoulousain-bucket-test';
    const objectName = 'new-Hubspot-Contact-' + moment().unix() + '.json';
    const objectData = JSON.stringify(mappers.mapHsContactToMdm(event));
    const objectType = 'application/json';
    
    try {
        // setup params for putObject
        const params = {
           Bucket: bucketName,
           Key: objectName,
           Body: objectData,
           ContentType: objectType,
        };
        const result = await s3.putObject(params).promise();
        console.log(`File uploaded successfully at https:/` + bucketName +   `.s3.amazonaws.com/` + objectName);

        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: result
            }),
        }

      } catch (error) {
        console.log('error', error);
        response = {
            statusCode: 500,
            body: JSON.stringify({
                message: error,
                input: error,
            }),
        };
      }
      return response;
}