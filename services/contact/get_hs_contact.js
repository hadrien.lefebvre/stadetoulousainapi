'use strict';
const request = require('request-promise');
const moment = require('moment');
module.exports.get_hs_contact = async (event) => {
    let eventBody = event
    let response;
    let requestBody;
    const HS_API_KEY = process.env.HS_API_KEY
    let url = `https://api.hubapi.com/crm/v3/objects/contacts/search`
    
    // TODO: Change for ID ?
    // get email from body and fetch the contact that match with the request
    // console.log('HHHHHHH eventBody -> ', eventBody);
    // const body = JSON.parse(eventBody.body);
    // console.log('HHHHHHH body -> ', body);
    // const contactEmail = body.email;
    
    // const body = JSON.parse(eventBody.body);
    // console.log('HHHHHHH body -> ', body);
    const contactEmail = eventBody.email;

    const formData = {
        filterGroups: [{
            filters: [{
                propertyName: "email",
                operator: "EQ",
                value: contactEmail
            }]
        }]
    }
    try {
        let options = {
            'method': 'POST',
            'url': url,
            'headers': {
                'Content-Type': 'application/json'
            },
            qs: { hapikey: "42022aef-533d-4178-a59e-e03f7030aae0"}, // TODO API KEY VARIABLE
            body: JSON.stringify(formData)
        };

        requestBody = await request(options, function(error, res) {
            if (error) throw new Error(error);
            response = res.body;
        });

        response = {
            requestBody: JSON.parse(requestBody),
            timestamp: moment().unix()
        }

    } catch (error) {
        console.log('ERROR'); // TO REMOVE
        response = {
            statusCode: 500,
            body: JSON.stringify({
                message: error,
                input: error,
            }),
        };

    };
    console.log(response)
    return response


}
