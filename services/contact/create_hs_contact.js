'use strict';
const mappers = require('../../mappers/map_contacts')
const request = require('request-promise');
const moment = require('moment');
module.exports.create_hs_contact = async (event) => {
    let eventBody = event
    let response;
    let requestBodyContact;
    const HS_API_KEY = process.env.HS_API_KEY
    let url = `https://api.hubapi.com/crm/v3/objects/contacts`

    // Create Hubspot Contact Object from body
    let hsContact = mappers.mapMdmContactToHs(eventBody);

    const formData = {
        properties: {
            ...hsContact
        }
    }
    try {
        let options = {
            'method': 'POST',
            'url': url,
            'headers': {
                'Content-Type': 'application/json'
            },
            qs: { hapikey: "42022aef-533d-4178-a59e-e03f7030aae0" }, // TODO Variable here
            body: JSON.stringify(formData)
        };

        requestBodyContact = await request(options, function(error, res) {
            if (error) throw new Error(error);
            response = res.body;
        });

        response = {
            requestBodyContact: JSON.parse(requestBodyContact),
            timestamp: moment().unix()
        }

    } catch (error) {
        console.log('ERROR');

        response = { 
            statusCode: 500,
            body: JSON.stringify({
                message: error,
                input: error,
            }),
        };

    };

    return response
}
