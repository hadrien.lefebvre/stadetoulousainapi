'use strict';

const request = require('request-promise');
const moment = require('moment');
module.exports = async () => {
    console.log('Authorizer was called');
    const CLIENT_ID = process.env.CLIENT_ID;
    const CLIENT_SECRET = process.env.CLIENT_SECRET;
    const REDIRECT_URI = process.env.REDIRECT_URI;
    const OAUTH_URL = process.env.OAUTH_URL;
    let response;
    let st_token;

    const oauthURL = OAUTH_URL + '?' +
        'grant_type=client_credentials' + '&' +
        'client_id=' + CLIENT_ID + '&' +
        'client_secret=' + CLIENT_SECRET + '&' +
        'redirect_uri=' + REDIRECT_URI;

    try {
        let options = {
            'method': 'POST',
            'url': oauthURL,
            'headers': {
                'Content-Type': 'application/json'
            },
        };

        st_token = await request(options, function(error, res) {
            if (error) throw new Error(error);
            response = res.body;

        });

        response = {
            st_token: JSON.parse(st_token),
            timestamp: moment().unix()
        }

    } catch (error) {
        console.log('ERROR');

        response = {
            statusCode: 500,
            body: JSON.stringify({
                message: error,
                input: error,
            }),
        };

    };
    return response

}